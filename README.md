#How to Build

$ cd findfiles

Get build tools

$ sudo apt-get install build-essential

Need QT utility qmake to generate Makefile, there two ways to get qmake:

1. Obtain qmake from package system.

$ sudo apt-get install qt5-qmake

OR

2. install QT from https://www.qt.io/,

then execute $ <QT installation path>/<architecture>/bin/qmake

$ /opt/Qt5.12.10/5.12.10/gcc_64/bin/qmake

Then use qmake to generate Makefile

$ qmake

Then use make to build.

$ make

Executable findfiles will be placed at the root of project.


#How to execute the program
1. Print CLI help information

$ ./findfiles --help

Usage:

      ./findfiles

             Start with UI mode

     ./findfiles --path <directory to search file>

            Start with CLI mode with --path specifying directory to show files information

     ./findfiles --help

            Show this help


2. Print files under specified path

$ ./findfiles --path /home/ubuntu2004/test

TotalSize:158978

Path:/home/ubuntu2004/test/GavinLin.tar.gz?FileSize:154882?LastModifiedTime:Mon May 24 19:02:32 2021

Path:/home/ubuntu2004/test/GavinLin?FileSize:4096?LastModifiedTime:Mon May 24 19:00:21 2021


3. Use UI to get file information under the specified directory.

$ ./findfiles
Then an UI will appear, choose the directory through "Browse" button, and press "Find" button, it will list the info.


4. Send GET request to get file information from server when running UI mode(no argument for execution).

$ ./findfiles

Start another terminal and type

$ curl -X GET 127.0.0.1:9081/queryStats -d "path=/home/ubuntu2004/test"

TotalSize:158978

Path:/home/ubuntu2004/test/GavinLin.tar.gz?FileSize:154882?LastModifiedTime:Mon May 24 19:02:32 2021

Path:/home/ubuntu2004/test/GavinLin?FileSize:4096?LastModifiedTime:Mon May 24 19:00:21 2021

Note that '?' is a separator for each information(Path, FileSize, LastModifiedTime)

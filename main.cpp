/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QApplication>
#include <iostream>
#include <string>
#include <pistache/endpoint.h>
//#include <pistache/http.h>
#include "window.h"
#include "filehandler.h"

string serializeFileInfo(vector<pair<string, fileStat*>> &stats) {
    long totalSize = 0;
    string fileInfo;
    for(auto it = stats.begin() ; it != stats.end() ; it++) {
        fileInfo += "Path:" + it->first + "?";
        fileInfo += "FileSize:";
        totalSize += it->second->st_size;
        fileInfo += to_string(it->second->st_size) + "?";
        fileInfo = fileInfo + "LastModifiedTime:" + ctime(&it->second->st_mtime);
        delete it->second;
    }
    fileInfo = "TotalSize:" + to_string(totalSize) + "\n" + fileInfo;
    return fileInfo;
}
struct FileStatHandler : public Pistache::Http::Handler {
    HTTP_PROTOTYPE(FileStatHandler)
    void onRequest(const Pistache::Http::Request& req, Pistache::Http::ResponseWriter response) override{
        string responseStats;
        //long totalSize = 0;
        if (req.resource() == "/queryStats")
        {
            if (req.method() == Pistache::Http::Method::Get)
            {
                string body = req.body();
                if(body.compare( 0, 5,"path=") == 0) {
                    string targetPath = body.substr(5, body.size()-5);
                    vector<pair<string, fileStat*>> stats = fileHandler::scanDir(targetPath);
                    responseStats = serializeFileInfo(stats);
                    /*
                    for(auto it = stats.begin() ; it != stats.end() ; it++) {
                        responseStats += "Path:" + it->first + "?";
                        responseStats += "FileSize:";
                        totalSize += it->second->st_size;
                        responseStats += to_string(it->second->st_size) + "?";
                        responseStats = responseStats + "LastModifiedTime:" + ctime(&it->second->st_mtime);
                    }
                    responseStats = "TotalSize:" + to_string(totalSize) + "\n" + responseStats;
                    */
                }
                else {
                    response.send(Pistache::Http::Code::Not_Found, "Only support body data: path=");
                }
            }
            else {
                response.send(Pistache::Http::Code::Not_Found, "Only support method: GET");
            }
        }
        else
        {
            response.send(Pistache::Http::Code::Not_Found, "Only support resource /queryStats");
        }
        response.send(Pistache::Http::Code::Ok, responseStats);
    }
};

/*
void runRestful() {
    Pistache::Address addr(Pistache::Ipv4::any(), Pistache::Port(9081));
    auto opts = Pistache::Http::Endpoint::options().threads(1);

    Pistache::Http::Endpoint server(addr);
    server.init(opts);
    server.setHandler(Pistache::Http::make_handler<FileStatHandler>());
    server.serve();
}
*/

//void runRestful(std::make_shared<Pistache::Http::Endpoint> server) {
void runRestful(Pistache::Http::Endpoint *server) {
    auto opts = Pistache::Http::Endpoint::options()
            .threads(1);
    server->init(opts);
    //server->serve();
    server->setHandler(Pistache::Http::make_handler<FileStatHandler>());
    server->serveThreaded();
}

void printHelp() {
    cout<<"Usage: ./findfiles"<<endl;
    cout<<"                   Start with UI mode"<<endl;
    cout<<"       ./findfiles --path <directory to search file>"<<endl;
    cout<<"                   Start with CLI mode with --path specifying directory to show files information"<<endl;
    cout<<"       ./findfiles --help"<<endl;
    cout<<"                   Show this help"<<endl;
    exit(0);
}

int main(int argc, char *argv[])
{
    for(int i = 1; i < argc; i++){
        if(!strcmp(argv[i], "-h") || !strcmp(argv[i], "--help") ){
          printHelp();
        }
        else if(!strcmp(argv[i], "-p") || !strcmp(argv[i], "--path")){
            if(i+1 >= argc) {
                cout<<"Please specify directory path after --path arugment."<<endl;
            }
            else {
                string dirPath(argv[i+1]);
                vector<pair<string, fileStat*>> stats = fileHandler::scanDir(dirPath);
                string fileInfo = serializeFileInfo(stats);
                cout<<fileInfo<<endl;
            }
            exit(0);
        }
    }

    //std::thread first (runRestful);
    Pistache::Address addr(Pistache::Ipv4::any(), Pistache::Port(9081));
    //auto server = std::make_shared<Pistache::Http::Endpoint>(addr);
    Pistache::Http::Endpoint *server = new Pistache::Http::Endpoint(addr);
    runRestful(server);

    QApplication app(argc, argv);
    Window window;
    window.show();
    int appResult = app.exec();

    //first.~thread();
    //first.join();
    std::cout << "Shutdowning server" << std::endl;
    server->shutdown();
    return appResult;
}

QT += widgets
requires(qtConfig(filedialog))

HEADERS       = window.h \
    filehandler.h
SOURCES       = main.cpp \
                filehandler.cpp \
                window.cpp
CONFIG += c++17
LIBS += -lpistache

# install
target.path = $$[QT_INSTALL_EXAMPLES]/widgets/dialogs/findfiles
INSTALLS += target

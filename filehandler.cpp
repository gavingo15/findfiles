#include "filehandler.h"
#include <iostream>>
using namespace std;

fileHandler::fileHandler()
{

}

bool fileHandler::sortByFileSize(const pair<string, fileStat*> &a, const pair<string, fileStat*> &b) {
    return ((a.second->st_size) > (b.second->st_size));
}

vector<pair<string, fileStat*>> fileHandler::scanDir(string dirPath) {
    vector<pair<string, fileStat*>> stats;
    for(auto& p: filesystem::directory_iterator(dirPath)) {
        string path = p.path();
        fileStat *s = new fileStat();
        lstat(path.c_str(), s);
        stats.push_back({path, s});
    }
    sort(stats.begin(), stats.end(), sortByFileSize);
    return stats;
}

#ifndef FILEHANDLER_H
#define FILEHANDLER_H
#include <string>
#include <vector>
#include <filesystem>
#include <algorithm> // for sort
#include <sys/types.h> // for stat
#include <sys/stat.h> // for stat
#include <unistd.h> // for stat
//#include <time.h> // for last modified time transformation
using namespace std;

typedef struct stat fileStat;
class fileHandler
{
private:
    static bool sortByFileSize(const pair<string, fileStat*> &a, const pair<string, fileStat*> &b);
public:
    fileHandler();
    static vector<pair<string, fileStat*>> scanDir(string dirPath);
};

#endif // FILEHANDLER_H
